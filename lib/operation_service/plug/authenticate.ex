defmodule OperationService.Plug.Authenticate do
  import Plug.Conn
  import OperationService.Router.Helpers
  import Phoenix.Controller

  import Ecto.Query

  alias OperationService.Repo

  def init(default), do: default

  def call(conn, _default) do
    token = Plug.Conn.get_req_header(conn, "authorization")

    authenticate(conn, token)
  end

  defp authenticate(conn, ["Bearer " <> token]) do
    authentication_service_address = System.get_env "AUTH_SERVICE_PORT_4000_TCP_ADDR"

    user = ConCache.get_or_store(:cache, token, fn() ->
      response = HTTPotion.post("http://#{authentication_service_address}:4000/authenticate",
      [headers: ["Authorization": "Bearer " <> token]])

      if response.status_code != 200 do
        unauthorized(conn)
      end

      response.body
    end) |> Poison.decode!

    unless user do
      unauthorized(conn)
    else
      assign(conn, :current_user, %{ id: user["id"], email: user["email"]})
    end
  end

  defp authenticate(conn, _token) do
    unauthorized(conn)
  end

  defp unauthorized(conn) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(OperationService.ChangesetErrorView, "error.json", changeset: %{"message"=> "you are not allowed!"})
    |> halt
  end
end
