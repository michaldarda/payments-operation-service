defmodule OperationService.Worker.Charge do
  import Ecto.Query

  alias OperationService.Account
  alias OperationService.Repo

  def perform(charge) do
    token = charge["token"]

    account = Repo.one!(
    from u in Account,
    where: u.token == ^token)

    Repo.update!(%{account | balance: account.balance + charge["amount"] })
  end
end
