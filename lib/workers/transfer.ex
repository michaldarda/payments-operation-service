defmodule OperationService.Worker.Transfer do
  import Ecto.Query

  alias OperationService.User
  alias OperationService.Account
  alias OperationService.AccountAction
  alias OperationService.Repo

  def perform(current_user_id, transfer) do
    current_user = (from u in User, where: u.id == ^current_user_id) |> Repo.one!

    amount = transfer["amount"]
    recipient_email_address = transfer["email"]

    account = from(a in Account,
    where: a.user_id == ^current_user.id) |> Repo.one!

    recipients_account = from(a in Account,
    join: u in User, on: a.user_id == u.id,
    where: u.email == ^recipient_email_address) |> Repo.one!

    unless (account.balance - transfer["amount"]) < 0 do
      Repo.transaction(fn ->
        account = Repo.update!(%{account | balance: account.balance - transfer["amount"] })
        recipients_account = Repo.update!(%{recipients_account | balance: recipients_account.balance + transfer["amount"] })

        %AccountAction{
          account_id: account.id,
          description: "You have transfered #{transfer["amount"]} N to #{recipient_email_address}"
        }
        |> Repo.insert!

        invalidate_cache!(current_user)
      end)
    end
  end

  defp invalidate_cache!(user) do
    ConCache.delete(:cache, "#{user.id}_account_balance")
    ConCache.delete(:cache, "#{user.id}_account_actions")
  end
end
