defmodule OperationService.ChargeControllerTest do
  use OperationService.ConnCase

  alias OperationService.Account

  setup do
    conn = conn()
            |> put_req_header("accept", "application/json")

    {:ok, conn: conn}
  end

  test "charges user account", %{conn: conn} do
    charge_params = %{
      "charge" => %{
        token: "john@example.com",
        amount: 20.0,
      }}

    # user = %User{
    #   email: "john@example.com",
    #   login: "johnexample",
    #   password: "johnexample",
    #   password_confirmation: "johnexample",
    #   crypted_password: "johnexamplef",
    #   token: "mytoken"}

    # user = user |> Repo.insert!

    %Account{
      balance: 10.0,
      token: "john@example.com",
      user_id: 1} |> Repo.insert!

    conn = conn()
            |> put_req_header("cert", "GENEROUS-BANK-1234567")
            |> put_req_header("accept", "application/json")
            |> put_req_header("content-type", "application/json")
            |> post "/charges", charge_params

    assert json_response(conn, 200)
  end
end
