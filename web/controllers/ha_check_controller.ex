defmodule OperationService.HaCheckController do
  use OperationService.Web, :controller

  def show(conn, _params) do
    conn |> text "OK"
  end
end
