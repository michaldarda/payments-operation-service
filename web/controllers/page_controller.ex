defmodule OperationService.PageController do
  use OperationService.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
