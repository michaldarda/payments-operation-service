defmodule OperationService.TransferController do
  use OperationService.Web, :controller

  plug OperationService.Plug.Authenticate

  def create(conn, %{ "transfer" => transfer }) do
    Task.async fn() ->
      OperationService.Worker.Transfer.perform(current_user(conn).id, transfer)
    end

    render conn, account_balance: %{ balance: 0.0 }
  end

  defp current_user(conn) do
    conn.assigns.current_user
  end
end
