defmodule OperationService.Router do
  use OperationService.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", OperationService do
    pipe_through :api

    get "/ha_check", HaCheckController, :show
    resources "/charges", ChargeController, only: [:create]
    post "/deposit", TransferController, :create
  end
end
