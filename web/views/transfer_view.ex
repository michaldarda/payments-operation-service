defmodule OperationService.TransferView do
  use OperationService.Web, :view

  def render("create.json", %{account_balance: account_balance}) do
    account_balance
  end
end
